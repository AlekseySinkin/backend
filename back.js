#!/usr/local/bin/node

var fs = require('fs');

var events = require('events');
var event  = new events.EventEmitter();
var net = require('net');


var DB;
var Port;

let LogLevel = 1;


var log = function (s = '') {
    console.log(s);
    }
}


function DBLoad(DB = 'db.json',) {
//=============================================================================================
  //Проверяем доступ к файлу на чтение
  fs.access(__dirname+ '/' + DB, fs.constants.F_OK | fs.constants.R_OK | fs.constants.W_OK, (err) => {
    if (err)
      throw 'File does not reading';
    }
  );

  // Загружаем файл
  return JSON.parse(trParameterValues = fs.readFileSync(__dirname + '\\' + DB,
    function (err, data) {
      if (err) throw console.error(err);
      }
    ).toString());

}


/*
================================================================================================
*/

// Количество устройств из командной строки. Должно быть только число иначе распарситься только первое попавшее число или значение по умолчанию = 1
if (process.argv.length > 2) {
  let port = process.argv[2].match(/[0-9]+/g);
  if (Array.isArray(port) && port.length > 0)
    Port = port[0];
  }

DB = DBLoad();  
  
var server = net.createServer((socket) => {

  socket.on('data', (data) => {

	let s = data.toString();
	let body = '';
	
	
	
	// GET /categories
	let Req = s.match(/GET \/categories /g);
	if (Req !== null && Req.length > 0) {
		
		let _DB = JSON.parse(JSON.stringify(DB));
		_DB.forEach(function(cat){
			cat.product_count = cat.product.length;
			delete cat.product;
			});
		body = JSON.stringify(_DB);
		log("asd")
		}
	else
	
	
	
	// POST /categories {category: { name: “Candy” }}	
	if (s.search("GET \/categories\\?id=") > -1) {
		Req = s.match(/GET \/categories\?id=([0-9])&name=(.+) /);
		log(Req)
		if (Req !== null && Req.length > 0) {
			DB.push(JSON.parse('{"id":'+Req[1]+', "name": "'+Req[2]+'","product": []}'));
			}
			
		body = JSON.stringify(DB);
		}
	else
	
	
	// GET /categories/ID/products
	if (s.search("GET \/categories/[0-9]+/products ") > -1) {
		Req = s.match(/\/categories\/([0-9])+\/products/);
		if (Req !== null && Req.length > 0) {
			Req = Req[1];
			DB.forEach(function(cat){
				if (cat.id == Req) {
					body = JSON.stringify(cat.product);
					return false;
				}
				})
			
			
			}
		
		}
	else 
		
	
	
	//GET /categories/ID/products?name="asd"&price=32.2
	if (s.search("GET \/categories/[0-9]+/products?name") > -1) {
		
		Req = s.match(/\/categories\/([0-9])+\/products\?(.+) HTTP/);
		if (Req !== null && Req.length > 0) {
			let product = Req[2].split("&");
			let exist = false;
			DB[Req[1]-1].product.forEach(function(prod){
				// Проверяем уникальность продукта
				if (prod.name == product[0].split("=")[1]) {
					exist = true;
					return false;
					}
				});
			
			// Продукт не найден, добавляем.
			if (!exist) {
				DB[Req[1]-1].product.push(JSON.parse(
				'{"name": "' + 
				product[0].split("=")[1] + 
				'", "price":'+ 
				product[1].split("=")[1] + '}'));
			}
		}
		
		body = JSON.stringify(DB);
	}	
	else
		
	
	
	//DELETE /products/ID
	if (s.search("GET /categories/[0-9]+/products\\?del") > -1) {
		
		Req = s.match(/GET \/categories\/([0-9]+)\/products\?del/);
		if (Req !== null && Req.length > 0) {
			
			DB.forEach(function(prod){
				if (prod.id == Req[1]) {
					prod.product.length = 0;
					}
			});
		}
		
		log(DB)
		body = JSON.stringify(DB);
		
		}
		
	
	
	let head = 'HTTP/1.1 200 OK\r\n' +
         'Content-Length: ' + body.length + '\r\n' +
         'Content-Type: text/json\r\n'+
         '\r\n'; 
		 
		 
    socket.write(head + body);
		});

  socket.on('end', () => {
		});

  socket.on('error', (err) => {
    log(err.toString());
    });

  });


server.on('error', (err) => {
    throw err;
    });

server.listen(Port, () => {
    });
